let nameTabs = document.querySelectorAll('.tabs li');

nameTabs.forEach(item => {
    item.addEventListener('click', () => {
        let activeTab = document.querySelector('.tabs li.active');
        let accurateData = document.querySelector(`.tab-content[data-tab-content = "${item.dataset.tabTrigger}"]`);

        //remove&add open
        document.querySelector('.tab-content.open').classList.remove('open');
        accurateData.classList.add('open');

        //remove&add active
        activeTab.classList.remove('active');
        item.classList.add('active');
    });
});

